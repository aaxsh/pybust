from requests_threads import AsyncSession
import sys
import argparse
import validators
import os.path
import time
import random

_START_ = r'''
 /$$$$$$$            /$$$$$$$                        /$$
| $$__  $$          | $$__  $$                      | $$
| $$  \ $$ /$$   /$$| $$  \ $$ /$$   /$$  /$$$$$$$ /$$$$$$
| $$$$$$$/| $$  | $$| $$$$$$$ | $$  | $$ /$$_____/|_  $$_/
| $$____/ | $$  | $$| $$__  $$| $$  | $$|  $$$$$$   | $$
| $$      | $$  | $$| $$  \ $$| $$  | $$ \____  $$  | $$ /$$
| $$      |  $$$$$$$| $$$$$$$/|  $$$$$$/ /$$$$$$$/  |  $$$$/
|__/       \____  $$|_______/  \______/ |_______/    \___/
           /$$  | $$
          |  $$$$$$/
           \______/

@author: aaxsh
@version: 1.0
'''

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def filecheck(filename):
    if filename != None and os.path.isfile(filename):
        print(f'[{bcolors.OKGREEN}OK{bcolors.ENDC}] Using {filename}')
        return filename
    else:
        print(f'[{bcolors.FAIL}FAIL{bcolors.ENDC}] Couldn\'t find {filename} and it seems to that test.txt doesn\'t exists!')
        sys.exit(0)

def urlcheck(_URL_):
    if not validators.url(_URL_):
        print(f'[{bcolors.FAIL}FAIL{bcolors.ENDC}] \'{_URL_}\' is not a valid URL. Please insert a legal target!')
        sys.exit(0)
    else:
        print(f'[{bcolors.OKGREEN}OK{bcolors.ENDC}] Using {_URL_}')

def command():
    ap = argparse.ArgumentParser()
    ap.add_argument('-t', '--target', required = True, help = f'{bcolors.BOLD}REQUIRED{bcolors.ENDC} Please Insert the target')
    ap.add_argument('-f', '--file', required = True, help = 'Your directory you want to use. If the file is in an other folder. Please give it also! test.txt ist set as the standart file!')
    ap.add_argument('-ua', '--useragent', required = False, help = 'A file, where User Agents are safe. They will be picked randomly.' )
    args = vars(ap.parse_args())
    return args

def dirname(file):
    with open(file) as file:
        names = file.read().splitlines()
        return names

def ua (list_ua):
    return random.choice(list_ua)

async def _main():
    urls = []
    found = 0
    items = len(names)
    start_time = time.time()
    for i, name in enumerate(names):
        url = _URL_ + name + "/"
        print(f'{bcolors.BOLD}{i}/{items} Checking keyword{bcolors.ENDC}: {name}', end = '\r')
        a = await session.get(url)
        if a.status_code == 200:
            print(f'<{bcolors.OKGREEN}200{bcolors.ENDC}> {url}')
            found += 1
            urls.append(url)
    print(f'[{bcolors.HEADER}OK{bcolors.ENDC}] Requests done succesfully!')

    if found == 0:
        print(f'[{bcolors.FAIL}FAIL{bcolors.ENDC}] Found nothing!')
    else:
        print(f'{bcolors.BOLD}Found: {found}{bcolors.ENDC}')
        print(urls)

args = command()
_URL_ = args['target']
urlcheck(_URL_)

filename = args['file']
filename = filecheck(filename)

ua = args['useragent']

list_ua = 'Opera/9.70 (Linux i686 ; U; en) Presto/2.2.1'
if ua != None:
    ua = filecheck(ua)
    list_ua = dirname(ua)

print(_START_)
print(f'[{bcolors.OKGREEN}OK{bcolors.ENDC}] Program started')

names = dirname(filename)

print(f'[{bcolors.OKGREEN}OK{bcolors.ENDC}] List loaded')
print(f'[{bcolors.HEADER}OK{bcolors.ENDC}] Starting requests!')

session = AsyncSession(len(names))

if __name__ == '__main__':
    session.run(_main)
